import axios from "axios";

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export const getAllCountry = () => {
  return axios.get("https://restcountries.com/v3.1/all");
};
