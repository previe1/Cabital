/**
 * 检查如如内容是否符合筛选要去
 * @param {*} itemData
 * @param {*} searchText
 * @returns 检查结果
 */
const checkSearch = (itemData, searchText) => {
  searchText = searchText.toLocaleUpperCase();
  const commonName = itemData.name.common.toLocaleUpperCase();
  const officialName = itemData.name.official.toLocaleUpperCase();
  const cca2 = itemData.cca2.toLocaleUpperCase();
  return (
    commonName.indexOf(searchText) >= 0 ||
    officialName.indexOf(searchText) >= 0 ||
    cca2.indexOf(searchText) >= 0
  );
};

/**
 * 根据筛选条件处理返回的国家
 * @param {*} countrys
 * @param {*} search
 * @param {*} region
 * @returns 返回符合条件的国家列表
 */
const filterCOuntry = (countrys, search, region) => {
  const REGION = region ? region.toLocaleUpperCase() : ""; //转为大大写，便于比较
  const list = [];
  countrys.forEach((el) => {
    // region存在，且当前元素相等，则跳过
    if (REGION && el.region.toLocaleUpperCase().indexOf(REGION) === -1) return;
    if (search && !checkSearch(el, search)) return;
    list.push(el);
  });
  return list;
};

export { filterCOuntry };
