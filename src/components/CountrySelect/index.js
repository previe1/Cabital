import React, { useState } from "react";
import OutlinedInput from "@mui/material/OutlinedInput";
import SearchIcon from "@mui/icons-material/Search";
import styles from "./index.module.css";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import "./select.css";
import { filterCOuntry } from "./filterCountry";

const Regions = ["Africa", "America", "Asia", "Europe", "Oceania"];

export default function CountrySelect({ countries, onSearch }) {
  const [region, setRegion] = useState("");
  const [search, setSearch] = useState("");
  const onInputChange = (e) => {
    setSearch(e.target.value);
    const list = filterCOuntry(countries, e.target.value, region);
    onSearch(list);
  };
  const onSelectChange = (e) => {
    setRegion(e.target.value);
    const list = filterCOuntry(countries, search, e.target.value);
    onSearch(list);
  };
  return (
    <section className={styles.SearchView}>
      <OutlinedInput
        className={styles.inputSelect}
        value={search}
        onChange={onInputChange}
        placeholder='search for a country...'
        startAdornment={<SearchIcon />}
      />

      <Select
        className={styles["select-region"]}
        displayEmpty
        value={region}
        onChange={onSelectChange}>
        <MenuItem disabled value=''>
          <em className={styles.selectPlacehoder}>Filter by Region</em>
        </MenuItem>
        {Regions.map((name) => (
          <MenuItem key={name} value={name}>
            {name}
          </MenuItem>
        ))}
      </Select>
    </section>
  );
}
