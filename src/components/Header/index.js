import React from "react";
import styles from "./index.module.css";
import BedtimeIcon from "@mui/icons-material/Bedtime";
import BedtimeOffIcon from "@mui/icons-material/BedtimeOff";

export default function Header({ isDark }) {
  return (
    <header className={styles.header}>
      <h2>Where in the world?</h2>
      <section className={styles.darkmode}>
        {isDark ? <BedtimeIcon sx={{ fontSize: 15 }} /> : <BedtimeOffIcon fontSize='small' />}
        <span className={styles.dartText}>{isDark ? "Dark" : "Light"} Mode</span>
      </section>
    </header>
  );
}
