import styles from "./index.module.css";
import React from "react";

function App(props) {
  const { list, onOpen } = props;

  return (
    <section className={styles.list}>
      {list.map((el) => (
        <section key={el.area + el.name.common} className={styles.item}>
          <section className={styles.inner} onClick={() => onOpen(el)}>
            <img src={el.flags.png} alt='' />
            <section className={styles.infomation}>
              <h3>{el.name.common}</h3>
              <p>
                <strong>Population:</strong>
                <span>{el.population}</span>
              </p>
              <p>
                <strong>Region:</strong>
                <span>{el.region}</span>
              </p>
              <p>
                <strong>Capital:</strong>
                <span>{el.capital && el.capital[0]}</span>
              </p>
            </section>
          </section>
        </section>
      ))}
    </section>
  );
}
export default App;
