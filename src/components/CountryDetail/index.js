import styles from "./index.module.css";
import React, { useEffect } from "react";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
  getNativeName,
  getTopLevelDomain,
  getCurrencies,
  getLanguages,
  getBorderCountrys,
} from "./utils";

const getScrollTop = () => {
  var scrollTop = 0;
  if (document.documentElement && document.documentElement.scrollTop) {
    scrollTop = document.documentElement.scrollTop;
  } else if (document.body) {
    scrollTop = document.body.scrollTop;
  }
  return scrollTop;
};

function App(props) {
  let { visible, countries, country, onClose } = props;

  useEffect(() => {
    const root = document.body;
    if (visible) {
      window._ModalAfterScrollTop = getScrollTop();
      root.style.position = "fixed";
      root.style.top = -window._ModalAfterScrollTop + "px";
      root.style.left = 0;
      root.style.right = 0;
      root.style.bottom = 0;
      root.style.overflowY = "hidden";
    } else {
      root.style.position = "";
      root.style.top = "";
      root.style.left = "";
      root.style.right = "";
      root.style.bottom = "";
      root.style.overflowY = "";
      window.scrollTo(0, window._ModalAfterScrollTop);
    }
  }, [visible]);

  if (!country) return null;

  return (
    <section className={styles.detail} style={{ display: visible ? "block" : "none" }}>
      <section>
        <button className={styles.back} onClick={onClose}>
          <ArrowBackIcon sx={{ fontSize: 22 }} />
          <span>Back</span>
        </button>
      </section>
      <div className={styles.content}>
        <section className={styles.flag}>
          <img src={country.flags.png} alt='' />
        </section>
        <div className={styles.rightInfo}>
          <section className={styles.infomation}>
            <h3>{country.name.common}</h3>
            <p>
              <strong>Native Name:</strong>
              <span>{getNativeName(country.name.nativeName)}</span>
            </p>
            <p>
              <strong>Population:</strong>
              <span>{country.population}</span>
            </p>
            <p>
              <strong>Region:</strong>
              <span>{country.region}</span>
            </p>
            <p>
              <strong>Sub Region:</strong>
              <span>{country.subregion}</span>
            </p>
            <p>
              <strong>Capital:</strong>
              <span>{country.capital && country.capital[0]}</span>
            </p>
          </section>
          <section className={styles.infomation}>
            <h3>&nbsp;</h3>
            <p>
              <strong>Top Level Domain:</strong>
              <span>{getTopLevelDomain(country.tld)}</span>
            </p>
            <p>
              <strong>Currencies:</strong>
              <span>{getCurrencies(country.currencies)}</span>
            </p>
            <p>
              <strong>Languages:</strong>
              <span>{getLanguages(country.languages)}</span>
            </p>
          </section>
          <section className={styles.borderCountry}>
            <strong>Border Countries:</strong>
            <section className={styles.borderList}>
              {getBorderCountrys(countries, country.borders).map((el) => (
                <span key={el} className={styles.borderItem}>
                  {el}
                </span>
              ))}
            </section>
          </section>
        </div>
      </div>
    </section>
  );
}
export default App;
