/**
 * 获取native 名字
 * @param {*} nativeName
 */
export const getNativeName = (nativeName) => {
  if (!nativeName) return "";
  const lastKey = Object.keys(nativeName).pop();
  return nativeName[lastKey].common;
};
/**
 * 获取Top Level Domain
 * @param {*} tld
 * @returns
 */
export const getTopLevelDomain = (tld) => {
  if (!tld) return "";
  return tld.join(",");
};
/**
 * 获取currencies
 * @param {*} currencies
 */
export const getCurrencies = (currencies) => {
  if (!currencies) return "";
  const list = [];
  for (const key in currencies) {
    if (Object.hasOwnProperty.call(currencies, key)) {
      const element = currencies[key];
      list.push(element.name);
    }
  }
  return list.join(",");
};
/**
 * 获取languages
 * @param {*} languages
 * @returns
 */
export const getLanguages = (languages) => {
  if (!languages) return "";
  return Object.values(languages).reverse().join(",");
};
/**
 * 获取border countrys
 * @param {*} countries
 * @param {*} borders
 * @returns
 */
export const getBorderCountrys = (countries, borders) => {
  const list = [];
  borders = borders || [];
  countries.forEach((el) => {
    if (borders.includes(el.cca2) || borders.includes(el.cca3) || borders.includes(el.cioc)) {
      list.push(el.name.common);
    }
  });
  return list;
};
