import "./App.css";
import { getAllCountry } from "./apis";
import React, { useCallback, useEffect, useRef, useState } from "react";
import CountryItem from "./components/CountryItem";
import CountrySelect from "./components/CountrySelect";
import Header from "./components/Header";
import CountryDetail from "./components/CountryDetail";

function App() {
  const [list, setList] = useState([]); // 显示的国家列表
  const [isDark, setDark] = useState(false);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);
  const [showCountry, setShowCountry] = useState(null);
  const allCountryData = useRef([]); // 缓存所有国家数据
  /**
   * 获取所有国家数据
   */
  const allCountry = useCallback(async () => {
    try {
      const data = await getAllCountry();
      setList(data);
      allCountryData.current = data;
      setLoading(false);
    } catch (error) {
      Promise.reject(error);
    }
  }, []);

  /**
   * 初始化页面数据
   */
  useEffect(() => {
    allCountry();
  }, [allCountry]);

  useEffect(() => {
    let media = window.matchMedia("(prefers-color-scheme:dark)");
    setDark(media.matches);
    //监听样式切换
    let callback = (e) => {
      let prefersDarkMode = e.matches;
      setDark(prefersDarkMode);
    };

    if (typeof media.addEventListener === "function") {
      media.addEventListener("change", callback);
    }

    return () => {
      return media.removeEventListener("change", callback);
    };
  }, []);

  const openDetail = (el) => {
    setVisible(true);
    setShowCountry(el);
  };

  return (
    <div className='App'>
      <Header isDark={isDark} />
      <CountrySelect countries={allCountryData.current} onSearch={setList} />
      <CountryItem list={list} onOpen={openDetail} />
      <CountryDetail
        visible={visible}
        countries={allCountryData.current}
        country={showCountry}
        onClose={() => setVisible(false)}
      />
      {loading && (
        <div className='loading-ring'>
          Loading
          <span></span>
        </div>
      )}
      {!loading && list.length === 0 && (
        <div className='search-empty'>Search results are empty</div>
      )}
    </div>
  );
}

export default App;
